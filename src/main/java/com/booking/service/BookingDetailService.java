package com.booking.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.catalina.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.booking.dto.BusDeatilsDto;
import com.booking.exceptions.NoDataFoundException;
import com.booking.exceptions.UserNotFoundException;
import com.booking.model.BookingDetails;
import com.booking.model.BusDetails;
import com.booking.model.UserRegistration;
import com.booking.repository.BookingDetailsRepository;
import com.booking.repository.BusDetailsRepository;

import com.booking.repository.UserRepository;

@Service
public class BookingDetailService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	BookingDetailsRepository bookingDetailsRepository;

	@Autowired
	BusDetailsRepository busdetailsRepository;

	BusDetails busDetails = new BusDetails();
	BookingDetails bookingDetails = new BookingDetails();
	long busId;

	public BookingDetails saveBusDetails(BookingDetails bookingDetails) {
		// BeanUtils.copyProperties(busDetails, busDetails);
		if (busDetails.getSeatsAvailable() > bookingDetails.getNumberOfPassengers()) {
			return bookingDetailsRepository.save(bookingDetails);
		} else {
			throw new NoDataFoundException("Enter valid BUS DETAILS");
		}

	}

	/*
	 * public BookingDetails add(BookingDetails bookingDetails) {
	 * 
	 * return detailsRepository.saveAndFlush(bookingDetails); }
	 * 
	 * int availableSeat = bus.getSeatsAvailable(); int numberOfSeats =
	 * bus.getTotalSeats(); int travellingPassenger =
	 * bookingDetails.getNumberOfPassengers();
	 * 
	 * bus = busdetailsRepository.findById(busId).get(); if (availableSeat <
	 * numberOfSeats) { if (availableSeat > travellingPassenger) {
	 * detailsRepository.saveAndFlush(bookingDetails);
	 * 
	 * }else {
	 * 
	 * throw new NoDataFoundException("check availability"); } } return
	 * bookingDetails; }
	 */
}
