package com.booking.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.booking.model.BusDetails;

@Repository
public interface BusDetailsRepository extends JpaRepository<BusDetails, Long> {

	public List<BusDetails> findBySourceAndDestinationAndDay(String source, String destination, String day);
}
