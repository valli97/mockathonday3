package com.booking.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.booking.model.UserRegistration;

@Repository
public interface UserRepository extends JpaRepository<UserRegistration, Long> {
	public Optional<UserRegistration> findByEmailIdAndPassword(String emailId, String password);
}
