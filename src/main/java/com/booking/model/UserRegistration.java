package com.booking.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class UserRegistration {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long userId;

	private String userName;

	@Column(unique = true)
	private String emailId;

	private String password;

	private String mobileNumber;
	/*
	 * @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch =
	 * FetchType.LAZY) private List<BookingDetails> booking;
	 */

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	/*
	 * @JsonManagedReference public List<BookingDetails> getBooking() { return
	 * booking; }
	 * 
	 * public void setBooking(List<BookingDetails> booking) { this.booking =
	 * booking; }
	 */

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}
