package com.booking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.booking.dto.ResponseDto;
import com.booking.dto.UserRegistrationDto;
import com.booking.model.UserRegistration;
import com.booking.service.UserRegistrationService;

@RestController
public class UserRegistrationController {

	@Autowired
	UserRegistrationService userRegistrationService;
	ResponseDto responseDto = new ResponseDto();

	@PostMapping("/user")

	public ResponseEntity<ResponseDto> userRegistration(@RequestBody UserRegistrationDto userRegistrationDto) {
		userRegistrationService.saveUserRegistration(userRegistrationDto);
		responseDto.setMessage("User Registered Successfully");
		return new ResponseEntity<>(responseDto, HttpStatus.CREATED);
	}

	@GetMapping("/user1")
	public ResponseEntity<UserRegistration> userDetails(Long id) {
		return new ResponseEntity<UserRegistration>(userRegistrationService.passengerDetails(id), HttpStatus.OK);
	}
}
