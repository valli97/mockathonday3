package com.booking.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.booking.dto.BusDeatilsDto;
import com.booking.dto.ResponseBusDeatilsDto;
import com.booking.service.BusDetailsService;

@RestController
public class BusDetailsController {

	@Autowired
	BusDetailsService busDetailsService;
	ResponseBusDeatilsDto responsebusdeatilsDto = new ResponseBusDeatilsDto();

	@PostMapping("/Avalability")
	public ResponseEntity<ResponseBusDeatilsDto> searchByBusDetails(@RequestBody BusDeatilsDto busDetailsDto) {

		busDetailsService.searchDetails(busDetailsDto);
		responsebusdeatilsDto.setMessage("For your souce and destination seats are available");

		return new ResponseEntity<>(responsebusdeatilsDto, HttpStatus.OK);

	}

	@PostMapping("/bookBus")
	public ResponseEntity<ResponseBusDeatilsDto> AddByBusDetails(@RequestBody BusDeatilsDto busDetailsDto) {
		busDetailsService.saveBusDetails(busDetailsDto);
		responsebusdeatilsDto.setMessage("YOU have add your bus deatils Successfully");
		return new ResponseEntity<>(responsebusdeatilsDto, HttpStatus.OK);
	}

}
