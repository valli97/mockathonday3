package com.booking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.booking.dto.ResponseDto;
import com.booking.exceptions.NoDataFoundException;
import com.booking.model.BookingDetails;
import com.booking.service.BookingDetailService;

@RestController
public class BusBookingController {

	@Autowired
	BookingDetailService bookingDetailService;

	ResponseDto responseDto=new ResponseDto();

	@PostMapping("/book")
	public ResponseEntity<ResponseDto> Bookingbus(@RequestBody BookingDetails bookingDetails) {

		BookingDetails book = bookingDetailService.saveBusDetails(bookingDetails);
		responseDto.setMessage("You booking Confirm...!!!!");
		return new ResponseEntity<ResponseDto>(responseDto,HttpStatus.OK);
		}
}
