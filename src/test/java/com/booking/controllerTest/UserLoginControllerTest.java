package com.booking.controllerTest;

import static org.junit.Assert.assertEquals;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.booking.controller.UserLoginController;
import com.booking.dto.ResponseDto;
import com.booking.dto.UserLoginDto;
import com.booking.model.UserRegistration;
import com.booking.service.UserLoginService;


@RunWith(MockitoJUnitRunner.class)
public class UserLoginControllerTest {
	@InjectMocks
	UserLoginController userLoginController;

	@Mock
	UserLoginService userLoginService;

	@Test
	public void TestcheckLoginByUserIdForPositive() {

		UserRegistration regdto = new UserRegistration();
		regdto.setUserId(1);
		regdto.setUserName("kumar");
		regdto.setPassword("kuara980");
		regdto.setEmailId("guru@gmail.com");
		regdto.setMobileNumber("8870448827");

		ResponseDto resdto = new ResponseDto();
		resdto.setMessage("success");

		UserLoginDto userlogindto = new UserLoginDto();
		userlogindto.setEmailId("dhaya12@gmail.com");
		userlogindto.setPassword("887045");

		Mockito.when(userLoginService.checkLoginByUserId(userlogindto)).thenReturn(Optional.of(regdto));

		ResponseEntity<ResponseDto> result = userLoginController.checkLoginByUserId(userlogindto);

		assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());

	}

}
