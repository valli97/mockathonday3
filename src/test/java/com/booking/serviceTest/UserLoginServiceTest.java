package com.booking.serviceTest;

import static org.junit.Assert.assertNotNull;

import java.util.Optional;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.booking.dto.UserLoginDto;
import com.booking.model.UserRegistration;
import com.booking.repository.UserRepository;
import com.booking.service.UserLoginService;

@RunWith(MockitoJUnitRunner.class)
public class UserLoginServiceTest {

	@InjectMocks
	UserLoginService userLoginService;

	@Mock
	UserRepository userRepository;

	@Test
	public void TestUserLoginServiceForPositive() {

		UserLoginDto userlogindto = new UserLoginDto();
		userlogindto.setEmailId("dhaya@gmail.com");
		userlogindto.setPassword("8870");

		UserRegistration userRegistration = new UserRegistration();
		userRegistration.setEmailId("dhaya@gmail.com");
		userRegistration.setMobileNumber("6347459383689");
		userRegistration.setPassword("8870");
		userRegistration.setUserId(1);
		userRegistration.setUserName("dhaya");

		Mockito.when(userRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.of(userRegistration));

		Optional<UserRegistration> res = userLoginService.checkLoginByUserId(userlogindto);
		assertNotNull(res);

	}

	@Test
	public void TestUserLoginServiceForNegative() {

		UserLoginDto userlogindto = new UserLoginDto();
		userlogindto.setEmailId("dhaya12@gmail.com");
		userlogindto.setPassword("887045");

		UserRegistration userRegistration = new UserRegistration();
		userRegistration.setEmailId("dhaya@gmail.com");
		userRegistration.setMobileNumber("6347459383689");
		userRegistration.setPassword("8870");
		userRegistration.setUserId(1);
		userRegistration.setUserName("dhaya");

		Mockito.when(userRepository.findByEmailIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(Optional.of(userRegistration));

		Optional<UserRegistration> res = userLoginService.checkLoginByUserId(userlogindto);
		assertNotNull(res);

	}

}
